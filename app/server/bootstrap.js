'use strict';
Meteor.startup(function() {
  if (Classes.find().count() === 0) {
    var sampleClasses = [{
      'when': '14.08.2015 17:45',
      'where': 'DanceFusion',
      'rate': 5000,
      'attendance': 23,
      'payDay': null,
      'substitute': null,
      'substitutePayDay': null,
      'substituteRate': null
    }];

    _.each(sampleClasses, function(c) {
      Classes.insert(c);
      console.log(c);
    });
  }
});
